<?php

namespace App\Controller;

use App\Entity\Admin\Article;
use App\Form\ArticleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController.
 */
class DefaultController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * DefaultController constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name = "article_index")
     *
     * @return Response
     */
    public function indexAction()
    {
        //достали mapper
        $em = $this->getDoctrine()->getManager();

        //выбрали репозиторий для entity Article и выбрали все записи из Article
        $articles = $em->getRepository(Article::class)->findAll();

        //выбор всех данных из article таблицы +рендер
        return $this->render('article/index.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route("/create", name="article_create")
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        //добавляем запрос в форму
        $form->handleRequest($request);

        //проверяем отправлена ли форма
        if ($form->isSubmitted() && $form->isValid()) {
            //добавляем сущность article что бы entity manager начал ей управлять
            $this->em->persist($article);

            //добавление сущности в базу
            $this->em->flush();

            return $this->redirectToRoute('article_index');
        }

        //возвращаем из create рендеринг template  (файл create.html.twig)
        return $this->render('article/create.html.twig', ['article' => $article, 'form' => $form->createView()]);
    }

    /**
     * @Route("/show/{id}", name="article_show")
     *
     * @return Response
     */
    public function showAction(int $id)
    {
        //достали mapper
        $em = $this->getDoctrine()->getManager();

        //выбрали репозиторий для entity Article и выбрали все записи из Article
        $article = $em->getRepository(Article::class)->find($id);

        return $this->render('article/show.html.twig', ['article' => $article]);
    }

    //        url           rout name - к нему привязываемся

    /**
     * @Route("/edit/{id}", name="article_edit")
     *
     *
     * @return Response
     */
    public function editAction(int $id, Request $request)
    {
        //достаем Article запись из базы
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);


        if (!$article) {
            throw $this->createNotFoundException('Article with ID'.$id.'not found!');
        }

        //передаем запись, которую достали, в функцию createForm
        $editForm = $this->createForm(ArticleType::class, $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            //добавляем сущность article что бы entity manager начал ей управлять
            $this->em->persist($article);

            //добавление сущности в базу
            $this->em->flush();

            return $this->redirectToRoute('article_show', ['id' => $id]);
        }

        //                                                   передача формы
        return $this->render('article/edit.html.twig', ['article' => $article, 'editForm' => $editForm->createView()]);
    }

    //создание ссылки

    /**
     * @Route("/delete/{id}", name="article_delete")
     *
     * @return Response
     */
    public function deleteAction(int $id)
    {
        //выбирает Article по полученному id
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Article::class)->find($id);

        if (!$article) {
            throw $this->createNotFoundException('Article with ID'.$id.'not found!');
        }

        //удаляем доктриной запись из базы
        $this->em->remove($article);
        $this->em->flush();

        //отправляем юзера на readAction
        return $this->redirectToRoute('article_index');
    }
}
